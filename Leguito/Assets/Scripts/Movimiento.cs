using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento : MonoBehaviour
{
    public Animator controller;
    public Rigidbody rb;

    public float currentSpeed;
    public float moveRotate;

    [Header("Run")]
    public float runSpeed;

    [Header("Jump")]
    public bool is_Grounded = false;
    public bool jumpTrue = false;
    public float jumpForce = 5f;

    public CheckPoints checkPoint;

    public AudioClip audioFXSalto;

    /*public AudioClip audioFX;
    AudioSource.PlayClipAtPoint(audioFX, gameObject.transform.position);*/

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void Update()
    {
        Move();
        Jump();
        Run();
    }

    public void Move()
    {
        Vector3 moveDirection = new Vector3(0, 0, Input.GetAxis("Vertical"));

        transform.Translate((moveDirection * currentSpeed) * Time.deltaTime);
        controller.SetFloat("Speed", moveDirection.z);


        transform.Rotate(new Vector3(0, Input.GetAxis("Horizontal") * moveRotate, 0));
    }

    public void Jump() 
    {
        Vector3 floor = transform.TransformDirection(Vector3.down);

        if (Physics.Raycast(transform.position, floor, 1f))
        {
            is_Grounded = true;
        }
        else
        {
            is_Grounded = false;
        }
        jumpTrue = Input.GetButtonDown("Jump");

        if (jumpTrue && is_Grounded)
        {
            AudioSource.PlayClipAtPoint(audioFXSalto, gameObject.transform.position);
            rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);

            controller.SetBool("Jump", true);
        }

        if (!jumpTrue && is_Grounded)
        {
            controller.SetBool("Jump", false);
        }
    }

    public void Run() 
    {
        if (Input.GetButtonDown("Fire3"))
        {
            currentSpeed += runSpeed;
        }
        if (Input.GetButtonUp("Fire3"))
        {
            currentSpeed -= runSpeed;
        }

        if (currentSpeed >= runSpeed)
        {
            controller.SetFloat("Speed", currentSpeed);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Lava")
        {
            transform.position = checkPoint.spawnPoint;
        }

        if (other.transform.tag == "CheckPoint")
        {
            checkPoint = other.GetComponent<CheckPoints>();
            checkPoint.gameObject.SetActive(false);
        }
    }
}
