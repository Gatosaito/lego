using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Score : MonoBehaviour
{
    private int score;
    public TextMeshProUGUI scoreText;
    public int cantidadMonedas;
    public string sceneName;

    private void Start()
    {
        score = 0;
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Puntaje")
        {
            score++;
            scoreText.text = "" + score;

            if (score == cantidadMonedas)
            {
                SceneManager.LoadScene(sceneName);
            }
        }
    }
}
